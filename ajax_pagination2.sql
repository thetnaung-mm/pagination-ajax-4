-- phpMyAdmin SQL Dump
-- version 3.4.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 20, 2013 at 11:10 AM
-- Server version: 5.5.20
-- PHP Version: 5.3.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `2my4edge`
--

-- --------------------------------------------------------

--
-- Table structure for table `pagination`
--

CREATE TABLE IF NOT EXISTS `pagination` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `designation` varchar(60) NOT NULL,
  `place` varchar(60) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `pagination`
--

INSERT INTO `pagination` (`id`, `name`, `designation`, `place`) VALUES
(1, 'Arunkumar Maha', 'Web developer', 'Tamilnadu'),
(2, 'Billgates', 'Microsoft', 'America'),
(3, 'Sachin tendulkar', 'Cricketer', 'India'),
(4, 'Rajinikanth', 'Actor', 'Tamilnadu'),
(5, 'A.R.Rahamnn', 'Musician', 'Tamilnadu'),
(6, 'Steve Jobs', 'Apple owner', 'America'),
(7, 'M S Dhoni', 'Cricketer', 'India'),
(8, 'Mark zuck', 'CEO Facebook', 'America'),
(9, 'A P J Abdul kalam', 'Scientist', 'India'),
(10, 'Illayaraja', 'Musician', 'Tamilnadu');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
